# Setup Managed Server to Configure with Ansible

#### Project Outline

In this project we will see how we can connect to two servers using our Ansible control machine

#### Lets get started

First lets install ansible

```
pip install ansible, pip show ansible
```


Now that we have Ansible installed, lets now create two digital ocean server droplets for it to connect to

![Image1](https://gitlab.com/FM1995/setup-managed-server-to-configure-with-ansible/-/raw/main/Images/Image1.png)

Since they are Ubuntu servers they will already have python installed

So lets ssh in to it and check

```
ssh root@46.101.7.145
```

![Image2](https://gitlab.com/FM1995/setup-managed-server-to-configure-with-ansible/-/raw/main/Images/Image2.png)

And check for python in the bin directory

```
ls /usr/bin/python3
```

Python is typically included in the default Ubuntu installation, so it's likely that Python 3 was installed automatically when the Ubuntu operating system was installed

![Image3](https://gitlab.com/FM1995/setup-managed-server-to-configure-with-ansible/-/raw/main/Images/Image3.png)

Now lets connect our Ansible control machine on to our remote servers

And we do that using a file called hosts also know as the inventory file

Here we can specify the IP, the private key file and the ansible user to connect to the servers

![Image4](https://gitlab.com/FM1995/setup-managed-server-to-configure-with-ansible/-/raw/main/Images/Image4.png)

We can use the below format to interact with the desired servers

```
ansible [pattern] -m [module] -a “[module options]”
```

The pattern is targeting hosts and groups so in our case it will ‘all’

-[module] is we can specify it to be ‘ping’

```
ansible all -I hosts -m ping
```

![Image5](https://gitlab.com/FM1995/setup-managed-server-to-configure-with-ansible/-/raw/main/Images/Image5.png)




